![We visualize our synthetic shape space by demonstrating the effect of moving along the individual factors to the
final shape.](images/Factors.png)
# The Freaky Torus
**Authors:** 
[Josua Sassen](https://josuasassen.com), 
[Klaus Hildebrandt](https://graphics.tudelft.nl/~klaus/), 
[Martin Rumpf](https://ins.uni-bonn.de/staff/rumpf), 
[Benedikt Wirth](https://www.uni-muenster.de/AMM/num/wirth/people/Wirth/index.html)

This repository contains the Python code to compute samples on our shape space *Freaky Torus* of
deformed tori, a synthetic shape space with factors $S^1 \times S^1 \times \mathbb{T}^2$.
The first factor, an $S^1$, controls the deformation of the latitudinal cross-section into a rotated ellipse.
The next factor, another $S^1$, controls the deformation of the longitudinal cross-section into a rotated ellipse.
The third factor, a two-dimensional flat torus $\mathbb{T}^2$, controls the position of a bump on the deformed torus.
Above, we visualize our synthetic shape space by demonstrating the effect of moving along the individual factors to the
final shape.
More details on the construction can be found in the appendix of our ICLR 2023 paper [*Parametrizing Product Shape
Manifolds by Composite Networks*](https://arxiv.org/abs/2302.14665).

Additional to the code, we also include the exact dataset that we used for the results of our paper in the `data` folder.

To create your own variation of the dataset, just run `python main.py`.
It requires the mesh of a torus of revolution with the right topology and the parametrization over $[0,1)$ saved in the 
texture coordinates.
We provide such a mesh generated using ParaView in `data/raw_torus.ply`.
To find out about the different options you can modify, simply run `python main.py -h`.
Running the tool will create one `.ply` file and eight `.npy` files:
 - `reference.ply` containing the mesh of the reference shape for $\mathcal{F}(\pi, \pi, \pi, \pi)$
 - `Coordinates.npy` containing a $4 \times N$ array of the points sampled from $[0, 2\pi)^4$ (parametrizing $S^1 \times S^1 \times \mathbb{T}^2$)
 - `Coordinates_{0,1,2}.npy` containing $1 \times N$ resp. $2 \times N$ arrays of the values of the sampled points for the individual factors
 - `VertexCoordinates.npy` containing a $3\# \mathrm{verts} \times N$ array of the vertex positions for the sampled shapes. Each vector of vertex positions is in column-major format, i.e. contains first all `x`-values, then all `y`-values, and finally all `z`-values.
 - `VertexCoordinates_{0,1,2}.npy` containing $3\# \mathrm{verts} \times N$ arrays of the vertex positions for the contributions of the individual factors to the shape, i.e. evaluating the parametrization with the parameters for all other factors set to the reference value $\pi$. 

These can then be used to train the parametrization or disentanglement method of your choice.

If you should have any questions, feel free to reach out to [Josua Sassen](https://josuasassen.com)!

### Requirements
This code uses the following packages
 - `numpy`
 - `scipy`
 - `openmesh`
 - `tqdm`

The repository contains the necessary files to set up a virtual environment using Poetry. Otherwise, all the packages
are available via pip.

### Attribution
If you use our dataset in a research paper, please cite our paper! You can use the following bibtex:
```
@inproceedings{SaHiRu23,
    title = {{Parametrizing Product Shape Manifolds by Composite Networks}},
    author = {Sassen, Josua and Hildebrandt, Klaus and Rumpf, Martin and Wirth, Benedikt},
    booktitle={The Eleventh International Conference on Learning Representations},
    year = {2023},
    archiveprefix = {arXiv},
    eprint = {2302.14665},
    url={https://openreview.net/forum?id=F_EhNDSamN}
}
```
Alternatively, one can also cite a persistent version of the dataset using
```
@data{SaHiRu23b,
    author = {Sassen, Josua and Hildebrandt, Klaus and Rumpf, Martin and Wirth, Benedikt},
    publisher = {bonndata},
    title = {{The Freaky Torus}},
    year = {2023},
    version = {V1},
    doi = {10.60507/FK2/LORXU7},
    url = {https://doi.org/10.60507/FK2/LORXU7}
}
```

### Acknowledgements
We came up with this synthetic shape space during the workshop *Mathematical Imaging and Surface Processing* 
at the Mathematisches Forschungsinstitut Oberwolfach, which we would like to thank for its hospitality!